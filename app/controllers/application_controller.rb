class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "<h1>hello</h1>"
  end

  def goodbye
    render html: "goodbye!"
  end
end
